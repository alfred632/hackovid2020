import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import * as firebase from 'firebase';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {
  private db: any;
  facts: Array<any>;
  cards: Array<any>;

  constructor(private navCtrl: NavController) {
    this.db = firebase.firestore();
    this.getAllDocuments('facts').then((e) => {
      this.facts = e;
      this.addNewCards(1); })
   }
  goToTab2() {
    this.navCtrl.navigateForward('/playgame'); }
  goToTab3() {
    this.navCtrl.navigateForward('/configuration'); }

  getAllDocuments(collection: string): Promise<any> {
    return new Promise((resolve, reject) => {
        this.db.collection(collection)
            .get()
            .then((querySnapshot) => {
                let arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id
                    console.log(obj)
                    arr.push(obj);
                });

                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                } else {
                    console.log("No such document!");
                    resolve(null);
                }


            })
            .catch((error: any) => {
                reject(error);
            });
    });
  }

  loadData() {
      this.getAllDocuments('facts').then((e) => {
        this.facts = e;
        console.log('Facts Loaded');
    });
    }

  ionViewWillEnter(){
    this.cards = [];
    this.cards.pop();
    this.addNewCards(1);
  }

  addNewCards(count: number) {
     for (let _i = 0; _i < count; _i++) {
      const aleatori = Math.floor(Math.random() * this.facts.length);
      this.cards.push(this.facts[aleatori]);
      console.log('Message number: ' + aleatori);
      console.log('Message number: ' + this.facts[aleatori].$key);
}
  }
}

