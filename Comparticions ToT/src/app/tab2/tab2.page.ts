import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import 'rxjs/rx';
import { NavController } from '@ionic/angular';
import {
  StackConfig,
  DragEvent,
  SwingStackComponent,
  ThrowEvent,
  Direction,
  SwingCardComponent
} from 'angular2-swing';
import { Observable } from 'rxjs';
import { AnalysisService, Idea } from 'src/app/analysis.service';
import 'firebase/firestore';
import * as firebase from 'firebase';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page {


  @ViewChild('myswing1', {static: false}) swingStack: SwingStackComponent;
  @ViewChildren('mycards1') swingCards: QueryList<SwingCardComponent>;

  ideas: Observable<Idea[]>;
  cards: Array<any>;
  // tslint:disable-next-line: variable-name
  existing_cards: Array<any>;
  stackConfig: StackConfig;
  recentCard = '';

  public messages: any;
  private db: any;
  model: any = {};
  isEditing = false;


  constructor(private http: HttpClientModule,
              private navCtrl: NavController,
              private ideaService: AnalysisService) {
    this.db = firebase.firestore();
    this.stackConfig = {
      throwOutConfidence: (offsetX, offsetY, element) => {
        const confX = Math.min(Math.abs(offsetX) / (element.offsetWidth / 2), 1);
        const confY = Math.min(Math.abs(offsetY) / (element.offsetHeight / 2), 1);
        return Math.max(confX, confY);
      },
      transform: (element, x, y, r) => {
        this.onItemMove(element, x, y, r);
      },
      throwOutDistance: (d) => {
        return 800;
      }
    };
    console.log('constructor');
  }

  loadData() {
    this.getAllDocuments('messages').then((e) => {
      this.messages = e;
      console.log('Data Loaded');
  });
  }

  updateMessage(obj) {
    this.model = obj;
    this.isEditing = true;
  }



  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {

    this.swingStack.throwin.subscribe((event: DragEvent) => {
      event.target.style.background = '';
    });
    this.getAllDocuments('messages').then((e) => {
      this.messages = e;
      console.log('Data Loaded');
      this.addNewCards(1);
  });
    this.cards = [];
  }


  // Called whenever we drag an element
  onItemMove(element, x, y, r) {
    const color = '';
    const abs = Math.abs(x);
    const min = Math.trunc(Math.min(16 * 16 - abs, 16 * 16));


    element.style.background = color;
    element.style.transform = `translate3d(0, 0, 0) translate(${x}px, ${y}px) rotate(${r}deg)`;
  }

  // Connected through HTML


  onThrowOut(event: ThrowEvent) {
    if (event.throwDirection === Direction.UP) {
      this.markSuperThreat();
    } else if (event.throwDirection === Direction.RIGHT) {
      this.markThreat();
    } else {
      this.markTrick();
    }
  }

  markThreat() {
    this.cards.pop();
    this.recentCard = 'THREAT';
    console.log('Answer: THREAT');
    this.addNewCards(1);
  }

  markSuperThreat() {
    this.cards.pop();
    this.recentCard = 'SUPERTHREAT';
    console.log('Answer: SUPERTHREAT');
    this.addNewCards(1);
  }

  markTrick() {
    this.cards.pop();
    this.recentCard = 'TRICK';
    console.log('Answer: TRICK');
    this.addNewCards(1);
  }

  markNoidea() {
    console.log(this.messages.length);
    this.cards.pop();
    this.recentCard = 'UNKNOWN';
    console.log('Answer: UNKNOWN');
    this.addNewCards(1);
  }
  // Add new cards to our array
  addNewCards(count: number) {
    for (let _i = 0; _i < count; _i++) {
      const aleatori = Math.floor(Math.random() * this.messages.length);
      console.log(this.messages[aleatori].text);
      this.cards.push(this.messages[aleatori]);
      console.log('Message number: ' + aleatori);
      console.log('Message number: ' + this.messages[aleatori].$key);
    }

    // this.http.get('https://randomuser.me/api/?results=' + count)
    //   .map(data => data.json().results)
    //   .subscribe(result => {
    //     for (let val of result) {
    //       this.cards.push(val);
    //     }
    //   })
  }

  // http://stackoverflow.com/questions/57803/how-to-convert-decimal-to-hex-in-javascript
  decimalToHex(d, padding) {
    let hex = Number(d).toString(16);
    padding = typeof (padding) === 'undefined' || padding === null ? padding = 2 : padding;

    while (hex.length < padding) {
      hex = '0' + hex;
    }

    return hex;
  }
  goToTab1() {
    this.navCtrl.navigateForward('/dashboard');
  }

  getAllDocuments(collection: string): Promise<any> {
    return new Promise((resolve, reject) => {
        this.db.collection(collection)
            .get()
            .then((querySnapshot) => {
                let arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id
                    console.log(obj)
                    arr.push(obj);
                });

                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                } else {
                    console.log("No such document!");
                    resolve(null);
                }


            })
            .catch((error: any) => {
                reject(error);
            });
    });
}

updateDocument(collectionName: string, docID: string, dataObj: any): Promise<any> {
  return new Promise((resolve, reject) => {
      this.db
          .collection(collectionName)
          .doc(docID)
          .update(dataObj)
          .then((obj: any) => {
              resolve(obj);
          })
          .catch((error: any) => {
              reject(error);
          });
  });
}

}
