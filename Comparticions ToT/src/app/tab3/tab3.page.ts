import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import * as firebase from 'firebase';
import { AlertController } from '@ionic/angular';
import { AuthenticateService } from '../auth.service';
import { ProfileService } from '../profile.service';
import { Router } from '@angular/router';
import { Timestamp } from 'rxjs/rx';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  public userProfile: any;
  public currentUser: firebase.User;
  public dob: any;
  public where: any;
  private db: any;
  facts: Array<any>;
  cards: Array<any>;

  constructor(private navCtrl: NavController,
              private alertCtrl: AlertController,
              private authService: AuthenticateService,
              private profileService: ProfileService,
              private router: Router) {
                this.db = firebase.firestore();
                this.getAllDocuments('facts').then((e) => {
                    this.facts = e;
                    this.addNewCards(1); });
                firebase.auth().onAuthStateChanged(user => {
                  if (user) {
                    this.currentUser = user;
                    this.userProfile = firebase.firestore().doc(`/userProfile/${user.uid}`);
                  }
                });
   }

  goToTab2() {
    this.navCtrl.navigateForward('/playgame'); }
  goToTab3() {
    this.navCtrl.navigateForward('/configuration'); }

  getAllDocuments(collection: string): Promise<any> {
    return new Promise((resolve, reject) => {
        this.db.collection(collection)
            .get()
            .then((querySnapshot) => {
                let arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id
                    console.log(obj)
                    arr.push(obj);
                });

                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                } else {
                    console.log("No such document!");
                    resolve(null);
                }


            })
            .catch((error: any) => {
                reject(error);
            });
    });
  }

  loadData() {
      this.getAllDocuments('facts').then((e) => {
        this.facts = e;
        console.log('Facts Loaded');
    });
    }

  ionViewWillEnter(){
    this.cards = [];
    this.cards.pop();
    this.addNewCards(1);
    this.profileService
    .getUserProfile()
    .get()
    .then(userProfileSnapshot => {
        this.userProfile = userProfileSnapshot.data();
        this.dob = userProfileSnapshot.data().dob;
    });
  }

  addNewCards(count: number) {
     for (let _i = 0; _i < count; _i++) {
      const aleatori = Math.floor(Math.random() * this.facts.length);
      this.cards.push(this.facts[aleatori]);
      console.log('Message number: ' + aleatori);
      console.log('Message number: ' + this.facts[aleatori].$key);
}
  }
  goToTab1() {
    this.navCtrl.navigateForward('/dashboard');
  }

  getUserProfile(): firebase.firestore.DocumentReference {
    return this.userProfile; }

  updateDOB(dob: string): Promise<any> {
    return this.userProfile.update({ dob });
  }
}

