// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA_0363311lAaFWQE8wEKkOeGLCFT8rX1U',
    authDomain: 'trick-or-threat-67ecd.firebaseapp.com',
    databaseURL: 'https://trick-or-threat-67ecd.firebaseio.com',
    projectId: 'trick-or-threat-67ecd',
    storageBucket: '',
    messagingSenderId: '790095967471',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
