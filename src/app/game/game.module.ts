import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { GamePage } from "./game.page";
import { ExploreContainerComponentModule } from "../main/explore-container/explore-container.module";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([
      { path: "", component: GamePage },
      {
        path: "photo",
        loadChildren: () =>
          import("./photo/photo.module").then((m) => m.PhotoPageModule),
      },
      {
        path: "task1",
        loadChildren: () =>
          import("./tasks/task1/task1.module").then((m) => m.Task1PageModule),
      },
      {
        path: "task2",
        loadChildren: () =>
          import("./tasks/task2/task2.module").then((m) => m.Task2PageModule),
      },
      {
        path: "task3",
        loadChildren: () =>
          import("./tasks/task3/task3.module").then((m) => m.Task3PageModule),
      },
      {
        path: "task4",
        loadChildren: () =>
          import("./tasks/task4/task4.module").then((m) => m.Task4PageModule),
      },
      {
        path: "task5",
        loadChildren: () =>
          import("./tasks/task5/task5.module").then((m) => m.Task5PageModule),
      },
      {
        path: "task6",
        loadChildren: () =>
          import("./tasks/task6/task6.module").then((m) => m.Task6PageModule),
      },
      {
        path: "task7",
        loadChildren: () =>
          import("./tasks/task7/task7.module").then((m) => m.Task7PageModule),
      },
      {
        path: "task8",
        loadChildren: () =>
          import("./tasks/task8/task8.module").then((m) => m.Task8PageModule),
      },
      {
        path: "task9",
        loadChildren: () =>
          import("./tasks/task9/task9.module").then((m) => m.Task9PageModule),
      },
      
    ]),
  ],
  declarations: [GamePage],
})
export class GamePageModule {}
