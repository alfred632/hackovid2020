class Task {
  constructor(public imgSrc: string, public taskUrl: string) {}
}

export const TASKS: Task[] = [
  {
    imgSrc: "assets/images/game1/planet1.png",
    taskUrl: "game/task1",
  },
  {
    imgSrc: "assets/images/game1/planet2.png",
    taskUrl: "game/task2",
  },
  {
    imgSrc: "assets/images/game1/planet3.png",
    taskUrl: "game/task3",
  },
  {
    imgSrc: "assets/images/game1/planet4.png",
    taskUrl: "game/task4",
  },
  {
    imgSrc: "assets/images/game1/planet5.png",
    taskUrl: "game/task5",
  },
  {
    imgSrc: "assets/images/game1/planet6.png",
    taskUrl: "game/task6",
  },
  {
    imgSrc: "assets/images/game1/planet7.png",
    taskUrl: "game/task7",
  },
  {
    imgSrc: "assets/images/game1/planet8.png",
    taskUrl: "game/task8",
  },
  {
    imgSrc: "assets/images/game1/planet9.png",
    taskUrl: "game/task9",
  },
];
