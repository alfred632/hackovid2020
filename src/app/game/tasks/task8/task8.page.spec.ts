import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Task8Page } from './task8.page';

describe('Tab1Page', () => {
  let component: Task8Page;
  let fixture: ComponentFixture<Task8Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Task8Page],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Task8Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
