import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-task1',
  templateUrl: 'task1.page.html',
  styleUrls: ['task1.page.scss']
})
export class Task1Page {
  imageSource = undefined
  textSource = undefined

  constructor(public navCtrl: NavController) {
    let avatarIdx = localStorage.getItem("avatarIdx")
    if (avatarIdx) {
      this.imageSource = "assets/avatars/" + avatarIdx + ".png"
    }

    let task1Text = localStorage.getItem("task1Text")
    if (task1Text)
      this.textSource = task1Text
  }

  backButton() {
    this.navCtrl.pop()
  }
  
  setAvatar(index: number){
    localStorage.setItem("avatarIdx", String(index))
    this.imageSource = "assets/avatars/" + index + ".png"
  }

  onInputText(text: string) {
    localStorage.setItem("task1Text", text)
  }
}