import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { PhotoService } from '../../../services/photo.service';
import { NavController } from '@ionic/angular';
import 'firebase/firestore';
import * as firebase from 'firebase';

@Component({
  selector: 'app-task2',
  templateUrl: 'task2.page.html',
  styleUrls: ['task2.page.scss']
})
export class Task2Page {
  imageSource = undefined
  private db: any;
  public aventures: any;
  public dbinfo: any;
  text1: any;
  text2: any;
  constructor(public photoService: PhotoService, public actionSheetController: ActionSheetController, public navCtrl: NavController) {
    this.db = firebase.firestore();
    let avatarIdx = localStorage.getItem("avatarIdx")
    if (avatarIdx) {
      this.imageSource = "assets/avatars/" + avatarIdx + ".png"
    }

    let photoText = document.getElementById('PhotoText')
    if (photoText && this.photoService.isThereAPhoto())
      photoText.innerText = "Esborra primer la imatge abans de fer una nova"
    else if (photoText)
      photoText.innerText = "Prem el botó quan estiguis llest!"
  }

  ngOnInit() {
    this.photoService.loadSaved();
  }

  public async showActionSheet(photo, position) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
         }
      }]
    });
    await actionSheet.present();
  }

  backButton() {
    this.navCtrl.pop()
  };

  getAllDocuments(collection: string): Promise<any> {
    return new Promise((resolve, reject) => {
        this.db.collection(collection)
            .get()
            .then((querySnapshot) => {
                let arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id
                    console.log(obj)
                    arr.push(obj);
                });

                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                } else {
                    console.log("No such document!");
                    resolve(null);
                }


            })
            .catch((error: any) => {
                reject(error);
            });
    });
}
ngAfterViewInit() {
    this.getAllDocuments('aventures').then((e) => {
      this.aventures = e;
      this.text1 = e[1].textfoto1;
      this.text2 = e[1].textfoto2;
      console.log('Data Loaded');
  });
  }

  tryCapturePhoto() {
    let photoText = document.getElementById('PhotoText')
    if (photoText && this.photoService.isThereAPhoto())
      photoText.innerText = "Esborra primer la imatge abans de fer una nova"
    else if (photoText) {
      photoText.innerText = "Prem el botó quan estiguis llest!"
      this.photoService.addNewToGallery()
    }
  }
}
