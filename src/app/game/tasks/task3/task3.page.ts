import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import 'firebase/firestore';
import * as firebase from 'firebase';

@Component({
  selector: 'app-task3',
  templateUrl: 'task3.page.html',
  styleUrls: ['task3.page.scss']
})
export class Task3Page {
  imageSource = undefined

  private db: any;
  public task3Text: string;
  private task3Solution: string;
  public resultColor: string;
  public warningTextHidden: boolean = true;

  constructor(public navCtrl: NavController) {
    this.db = firebase.firestore();
    let avatarIdx = localStorage.getItem("avatarIdx")
    if (avatarIdx) {
      this.imageSource = "assets/avatars/" + avatarIdx + ".png"
    }
  }

  getAllDocuments(collection: string): Promise<any> {
    return new Promise((resolve, reject) => {
        this.db.collection(collection)
            .get()
            .then((querySnapshot) => {
                let arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id
                    console.log(obj)
                    arr.push(obj);
                });

                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                } else {
                    console.log("No such document!");
                    resolve(null);
                }


            })
            .catch((error: any) => {
                reject(error);
            });
    });
  }

  ngAfterViewInit() {
      this.getAllDocuments('aventures').then((e) => {
        let texts = []
        texts.push(e[1].textdex1) 
        texts.push(e[1].textdex2) 
        texts.push(e[1].textdex3) 
        texts.push(e[1].textdex4) 
        this.task3Text = texts[Math.floor(Math.random() * 4)]
        this.task3Solution = e[1].textdex5
        console.log('Data Loaded');
    });
  }

  backButton() {
    this.navCtrl.pop()
  }

  checkAnswer(){
    this.warningTextHidden = false
    let answer = document.getElementById("answerString").firstElementChild.innerHTML
    let resultTextElement = document.getElementById("resultString")
    console.log(document.getElementById("answerString"))
    console.log(answer.toUpperCase())
    console.log(this.task3Solution.toUpperCase())
    if (answer && answer.toUpperCase() === this.task3Solution.toUpperCase()){
      resultTextElement.innerText = "Resposta correcte, molt bé!"
      this.resultColor = "success"
      console.log("success")
    }
    else {
      resultTextElement.innerText = "Resposta incorrecte, torna-ho a provar."
      this.resultColor = "danger"
    }
  }
}