import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-task9',
  templateUrl: 'task9.page.html',
  styleUrls: ['task9.page.scss']
})
export class Task9Page {
  imageSource = undefined

  constructor(public navCtrl: NavController) {
    let avatarIdx = localStorage.getItem("avatarIdx")
    if (avatarIdx) {
      this.imageSource = "assets/avatars/" + avatarIdx + ".png"
    }
  }

  backButton() {
    this.navCtrl.pop()
  }
}