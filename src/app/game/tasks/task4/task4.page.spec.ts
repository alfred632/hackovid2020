import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Task4Page } from './task4.page';

describe('Tab1Page', () => {
  let component: Task4Page;
  let fixture: ComponentFixture<Task4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Task4Page],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Task4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
