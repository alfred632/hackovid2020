import { Component } from "@angular/core";
import { NavController } from "@ionic/angular";
import { TASKS } from "./tasks";

@Component({
  selector: "app-game",
  templateUrl: "game.page.html",
  styleUrls: ["game.page.scss"],
})
export class GamePage {
  tasks = TASKS;

  constructor(public navCtrl: NavController) {}

  backButton() {
    this.navCtrl.pop();
  }

  forwardTo(url: string) {
    this.navCtrl.navigateForward(url);
  }
}
