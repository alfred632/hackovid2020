import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Task9Page } from './task9.page';

describe('Tab1Page', () => {
  let component: Task9Page;
  let fixture: ComponentFixture<Task9Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Task9Page],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Task9Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
