import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-task3',
  templateUrl: 'task3.page.html',
  styleUrls: ['task3.page.scss']
})
export class Task3Page {
  imageSource = undefined

  constructor(public navCtrl: NavController) {
    let avatarIdx = localStorage.getItem("avatarIdx")
    if (avatarIdx) {
      this.imageSource = "assets/avatars/" + avatarIdx + ".png"
    }
  }

  backButton() {
    this.navCtrl.pop()
  }
}