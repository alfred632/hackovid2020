import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Task3Page } from './task3.page';

describe('Tab1Page', () => {
  let component: Task3Page;
  let fixture: ComponentFixture<Task3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Task3Page],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Task3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
