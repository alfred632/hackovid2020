import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { PhotoService } from '../../../services/photo.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-task2',
  templateUrl: 'task2.page.html',
  styleUrls: ['task2.page.scss']
})
export class Task2Page {
  imageSource = undefined

  constructor(public photoService: PhotoService, public actionSheetController: ActionSheetController, public navCtrl: NavController) {
    let avatarIdx = localStorage.getItem("avatarIdx")
    if (avatarIdx) {
      this.imageSource = "assets/avatars/" + avatarIdx + ".png"
    }

    let photoText = document.getElementById('PhotoText')
    if (photoText && this.photoService.isThereAPhoto())
      photoText.innerText = "Esborra primer la imatge abans de fer una nova"
    else if (photoText)
      photoText.innerText = "Prem el botó quan estiguis llest!"
  }

  ngOnInit() {
    this.photoService.loadSaved();
  }

  public async showActionSheet(photo, position) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
         }
      }]
    });
    await actionSheet.present();
  }

  backButton() {
    this.navCtrl.pop()
  }

  tryCapturePhoto() {
    let photoText = document.getElementById('PhotoText')
    if (photoText && this.photoService.isThereAPhoto())
      photoText.innerText = "Esborra primer la imatge abans de fer una nova"
    else if (photoText) {
      photoText.innerText = "Prem el botó quan estiguis llest!"
      this.photoService.addNewToGallery()
    }
  }
}