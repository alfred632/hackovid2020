import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Task2Page } from './task2.page';

describe('Tab1Page', () => {
  let component: Task2Page;
  let fixture: ComponentFixture<Task2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Task2Page],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Task2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
