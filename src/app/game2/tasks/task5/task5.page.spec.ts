import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Task5Page } from './task5.page';

describe('Tab1Page', () => {
  let component: Task5Page;
  let fixture: ComponentFixture<Task5Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Task5Page],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Task5Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
