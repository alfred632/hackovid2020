import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-task5',
  templateUrl: 'task5.page.html',
  styleUrls: ['task5.page.scss']
})
export class Task5Page {
  imageSource = undefined

  constructor(public navCtrl: NavController) {
    let avatarIdx = localStorage.getItem("avatarIdx")
    if (avatarIdx) {
      this.imageSource = "assets/avatars/" + avatarIdx + ".png"
    }
  }

  backButton() {
    this.navCtrl.pop()
  }
}