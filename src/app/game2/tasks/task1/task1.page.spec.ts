import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Task1Page } from './task1.page';

describe('Tab1Page', () => {
  let component: Task1Page;
  let fixture: ComponentFixture<Task1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Task1Page],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Task1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
