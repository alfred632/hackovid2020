import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-task4',
  templateUrl: 'task4.page.html',
  styleUrls: ['task4.page.scss']
})
export class Task4Page {
  imageSource = undefined

  constructor(public navCtrl: NavController) {
    let avatarIdx = localStorage.getItem("avatarIdx")
    if (avatarIdx) {
      this.imageSource = "assets/avatars/" + avatarIdx + ".png"
    }
  }

  backButton() {
    this.navCtrl.pop()
  }
}