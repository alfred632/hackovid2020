import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-task6',
  templateUrl: 'task6.page.html',
  styleUrls: ['task6.page.scss']
})
export class Task6Page {
  imageSource = undefined

  constructor(public navCtrl: NavController) {
    let avatarIdx = localStorage.getItem("avatarIdx")
    if (avatarIdx) {
      this.imageSource = "assets/avatars/" + avatarIdx + ".png"
    }
  }

  backButton() {
    this.navCtrl.pop()
  }
}