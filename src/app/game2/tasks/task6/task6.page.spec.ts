import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Task6Page } from './task6.page';

describe('Tab1Page', () => {
  let component: Task6Page;
  let fixture: ComponentFixture<Task6Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Task6Page],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Task6Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
