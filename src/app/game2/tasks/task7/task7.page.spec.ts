import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Task7Page } from './task7.page';

describe('Tab1Page', () => {
  let component: Task7Page;
  let fixture: ComponentFixture<Task7Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Task7Page],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Task7Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
