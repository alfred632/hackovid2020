class Task {
  constructor(public imgSrc: string, public taskUrl: string) {}
}

export const TASKS: Task[] = [
  {
    imgSrc: "assets/images/game2/illa.png",
    taskUrl: "game2/task1",
  },
  {
    imgSrc: "assets/images/game2/illa2.png",
    taskUrl: "game2/task2",
  },
  {
    imgSrc: "assets/images/game2/illa4.png",
    taskUrl: "game2/task3",
  },
  {
    imgSrc: "assets/images/game2/illa2.png",
    taskUrl: "game2/task4",
  },
  {
    imgSrc: "assets/images/game2/illa3.png",
    taskUrl: "game2/task5",
  },
  {
    imgSrc: "assets/images/game2/illa4.png",
    taskUrl: "game2/task6",
  },
  {
    imgSrc: "assets/images/game2/illa2.png",
    taskUrl: "game2/task7",
  },
  {
    imgSrc: "assets/images/game2/illa3.png",
    taskUrl: "game2/task8",
  },
  {
    imgSrc: "assets/images/game2/illa4.png",
    taskUrl: "game2/task9",
  },
];
