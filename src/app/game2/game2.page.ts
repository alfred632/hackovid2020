import { Component } from "@angular/core";
import { NavController } from "@ionic/angular";
import { TASKS } from "./game2Tasks";

@Component({
  selector: "app-game2",
  templateUrl: "game2.page.html",
  styleUrls: ["game2.page.scss"],
})
export class Game2Page {
  tasks = TASKS;

  constructor(public navCtrl: NavController) {}

  backButton() {
    this.navCtrl.pop();
  }

  forwardTo(url: string) {
    this.navCtrl.navigateForward(url);
  }
}
