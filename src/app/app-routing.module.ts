import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    loadChildren: () =>
      import("./login/login.module").then((m) => m.LoginPageModule),
  },
  {
    path: "register",
    loadChildren: () =>
      import("./register/register.module").then((m) => m.RegisterPageModule),
  },
  {
    path: "main",
    loadChildren: () =>
      import("./main/tabs/tabs.module").then((m) => m.TabsPageModule),
  },
  {
    path: "game",
    loadChildren: () =>
      import("./game/game.module").then((m) => m.GamePageModule),
  },
  {
    path: "game2",
    loadChildren: () =>
      import("./game2/game2.module").then((m) => m.Game2PageModule),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
