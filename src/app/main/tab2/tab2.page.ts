import { Component } from "@angular/core";
import { NavController } from "@ionic/angular";
import { AuthenticateService } from '../../services/auth.service';

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"],
  providers: [AuthenticateService]
})
export class Tab2Page {
  constructor(public navCtrl: NavController, private authService: AuthenticateService) {}

  postalCode: string = "";
  warningTextHidden: boolean = true;
  availableGamesHidden: boolean = true;

  searchButton() {
    //Check if code is null
    if (this.postalCode && this.postalCode.toString().length == 5){
      this.warningTextHidden = true;
      this.availableGamesHidden = false;
    }
    else {
      this.showErrorString()
      this.availableGamesHidden = true;
    }
  }

  showErrorString() {
    let error = this.postalCode ? "La longitud del codi postal no és vàlida." : "No s'ha introduït cap codi postal."
    document.getElementById("errorString").textContent = error
    this.warningTextHidden = false;
  }

  onInputText(value: number){
    if (value) {
      this.warningTextHidden = true
    }
  }

  playButton(url: string) {
    this.navCtrl.navigateForward(url);
  }

  logout() {
    this.authService.logoutUser()
    .then(res => {
      console.log(res);
      this.navCtrl.navigateBack('');
    })
    .catch(error => {
      console.log(error);
    })
  }
}
