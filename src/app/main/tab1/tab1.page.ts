import { Component } from '@angular/core';
import { NavController } from '@ionic/angular'
import { AuthenticateService } from '../../services/auth.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  providers: [AuthenticateService]
})
export class Tab1Page {

  constructor(public navCtrl: NavController, private authService: AuthenticateService) {}

  gameCode: string = "";
  warningTextHidden: boolean = true;
  validGameCode = 123456

  playButton() {
    //Check if code is null
    if (this.gameCode && parseInt(this.gameCode) == this.validGameCode){
      this.warningTextHidden = true;
      this.navCtrl.navigateForward('/game')
    }
    else 
      this.showErrorString()
  }

  showErrorString() {
    let error = this.gameCode ? "La partida no existeix." : "No s'ha introduït cap codi."
    document.getElementById("errorString").textContent = error
    this.warningTextHidden = false;
  }

  onInputText(value: number){
    if (value != null) {
      this.warningTextHidden = true
    }
  }
  
  logout(){
    this.authService.logoutUser()
    .then(res => {
      console.log(res);
      this.navCtrl.navigateBack('');
    })
    .catch(error => {
      console.log(error);
    })
  }
}