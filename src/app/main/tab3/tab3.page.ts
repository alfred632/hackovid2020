import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthenticateService } from '../../services/auth.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
  providers: [AuthenticateService]
})
export class Tab3Page {

  constructor(public navCtrl: NavController, private authService: AuthenticateService) {}

  logout() {
    this.authService.logoutUser()
    .then(res => {
      console.log(res);
      this.navCtrl.navigateBack('');
    })
    .catch(error => {
      console.log(error);
    })
  }

}
